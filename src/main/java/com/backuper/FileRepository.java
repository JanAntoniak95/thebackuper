package com.backuper;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface FileRepository extends CrudRepository<MyFile ,Integer>{
    public List<MyFile> findFilesByUserId(Integer user_id);
}
