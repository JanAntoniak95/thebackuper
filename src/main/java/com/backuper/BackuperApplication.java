package com.backuper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackuperApplication {
	public static void main(String[] args) {
		SpringApplication.run(BackuperApplication.class, args);
	}
}
