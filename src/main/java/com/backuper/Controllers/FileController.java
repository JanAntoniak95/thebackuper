package com.backuper.Controllers;

import com.backuper.FileService;
import com.backuper.MyFile;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @RequestMapping("/user/{id}/files")
    public List<MyFile> getAllFilesOfUser(@PathVariable Integer id) {
        return fileService.getAllFilesOfUser(id);
    }

    @RequestMapping("/file/default")
    public void addDefaultFileToDb() throws IOException, NoSuchAlgorithmException {
        fileService.addDefaultFile();
    }

    @RequestMapping("/file/{documentId}/download")
    public String download(@PathVariable("documentId")
                                   Integer id, HttpServletResponse response) {

        MyFile file = fileService.getFile(id);
        try {
            response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getFile().getName()+ "\"");
            OutputStream out = response.getOutputStream();
            response.setContentType("file");
            ByteArrayInputStream baos = new ByteArrayInputStream(file.getFileContent());
            IOUtils.copy(baos , out);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
