package com.backuper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private UserRepository ur;

    public List<MyFile> getAllFiles() {
        List<MyFile> files = new ArrayList<>();
        fileRepository.findAll()
                .forEach(files::add);
        return files;
    }

    public List<MyFile> getAllFilesOfUser(Integer user_id) {
        return fileRepository.findFilesByUserId(user_id);
    }

    //TODO add real user
    public void addDefaultFile() throws IOException, NoSuchAlgorithmException {
        MyFile mf = new MyFile("/home/jan/hit.mp3", ur.findOne(2));
        fileRepository.save(mf);
    }

    public MyFile getFile(Integer id) {
        return fileRepository.findOne(id);
    }
}
