package com.backuper;

import javax.persistence.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

@Entity
public class MyFile {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private User user;
    private File file;

    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(length=5677700)
    private byte[] fileContent;
    private String checkSum;

    public MyFile() {}

    public MyFile(String path, User user) throws IOException, NoSuchAlgorithmException {
        file = new File(path);
        this.user = user;
        FileInputStream fileInputStream = new FileInputStream(file);
        int byteLength = (int)file.length();
        fileContent = new byte[byteLength];
        fileInputStream.read(fileContent,0,byteLength);
        this.checkSum = countCheckSum();
    }

    private String countCheckSum() throws NoSuchAlgorithmException {
        byte[] a = MessageDigest.getInstance("MD5").digest(fileContent);
        return toHexString(a);
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for(int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyFile myFile = (MyFile) o;

        if (id != null ? !id.equals(myFile.id) : myFile.id != null) return false;
        if (user != null ? !user.equals(myFile.user) : myFile.user != null) return false;
        if (file != null ? !file.equals(myFile.file) : myFile.file != null) return false;
        if (!Arrays.equals(fileContent, myFile.fileContent)) return false;
        return checkSum != null ? checkSum.equals(myFile.checkSum) : myFile.checkSum == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(fileContent);
        result = 31 * result + (checkSum != null ? checkSum.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MyFile{" +
                "id=" + id +
                ", user=" + user +
                ", file=" + file +
                ", fileContent=" + Arrays.toString(fileContent) +
                ", checkSum='" + checkSum + '\'' +
                '}';
    }
}
